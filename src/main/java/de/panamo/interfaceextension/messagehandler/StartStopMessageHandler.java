package de.panamo.interfaceextension.messagehandler;

import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.panamo.interfaceextension.InterfaceExtension;
import org.java_websocket.WebSocket;

public class StartStopMessageHandler implements MessageHandler {
    private InterfaceExtension instance;

    public StartStopMessageHandler(InterfaceExtension instance) {
        this.instance = instance;
    }

    @Override
    public void handle(String message, String[] values, Document responseDocument, WebSocket webSocket, User user) {
        switch (message) {
            case "startserver":
                this.instance.getCloud().getScheduler().runTaskSync(() -> this.instance.getCloud().startGameServer(this.instance.getCloud().getServerGroup(values[0])));
                break;
            case "startproxy":
                this.instance.getCloud().getScheduler().runTaskSync(() -> this.instance.getCloud().startProxy(this.instance.getCloud().getProxyGroup(values[0])));
                break;
            case "stopserver":
                this.instance.getCloud().getScheduler().runTaskSync(() -> this.instance.getCloud().stopServer(values[0]));
                break;
            case "stopproxy":
                this.instance.getCloud().getScheduler().runTaskSync(() -> this.instance.getCloud().stopProxy(values[0]));
                break;
        }
    }
}

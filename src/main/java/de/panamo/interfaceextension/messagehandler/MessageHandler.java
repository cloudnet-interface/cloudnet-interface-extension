package de.panamo.interfaceextension.messagehandler;


import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import org.java_websocket.WebSocket;

public interface MessageHandler {

    void handle(String message, String[] values, Document responseDocument, WebSocket webSocket, User user);

}

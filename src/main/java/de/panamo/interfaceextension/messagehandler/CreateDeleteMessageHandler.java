package de.panamo.interfaceextension.messagehandler;


import de.dytanic.cloudnet.lib.map.WrappedMap;
import de.dytanic.cloudnet.lib.server.*;
import de.dytanic.cloudnet.lib.server.advanced.AdvancedServerConfig;
import de.dytanic.cloudnet.lib.server.template.Template;
import de.dytanic.cloudnet.lib.server.template.TemplateResource;
import de.dytanic.cloudnet.lib.server.version.ProxyVersion;
import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.dytanic.cloudnetcore.network.components.MinecraftServer;
import de.dytanic.cloudnetcore.network.components.ProxyServer;
import de.dytanic.cloudnetcore.network.components.WrapperMeta;
import de.dytanic.cloudnetcore.util.defaults.BasicProxyConfig;
import de.panamo.interfaceextension.InterfaceExtension;
import org.java_websocket.WebSocket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CreateDeleteMessageHandler implements MessageHandler {
    private InterfaceExtension instance;

    public CreateDeleteMessageHandler(InterfaceExtension instance) {
        this.instance = instance;
    }

    @Override
    public void handle(String message, String[] values, Document responseDocument, WebSocket webSocket, User user) {
        switch (message) {
            case "createservergroup":
                ServerGroup serverGroup = new ServerGroup(
                        values[0],
                        Collections.singletonList(values[1]),
                        true,
                        Integer.valueOf(values[2]),
                        Integer.valueOf(values[2]),
                        0,
                        false,
                        Integer.valueOf(values[3]),
                        Integer.valueOf(values[4]),
                        Integer.valueOf(values[5]),
                        180,
                        100,
                        100,
                        Integer.valueOf(values[6]),
                        ServerGroupType.valueOf(values[7]),
                        ServerGroupMode.valueOf(values[8]),
                        Arrays.asList(new Template(
                                "default",
                                TemplateResource.valueOf(values[9]),
                                null,
                                new String[0],
                                new ArrayList<>()
                        )),
                        new AdvancedServerConfig(false,
                                false, false, !ServerGroupMode.valueOf(values[8]).equals(ServerGroupMode.STATIC)));
                this.instance.getCloud().getConfig().createGroup(serverGroup);
                break;
            case "createproxygroup":
                ProxyGroup proxyGroup = new ProxyGroup(values[0], Collections.singletonList(values[1]), new Template(
                        "default",
                        TemplateResource.valueOf(values[2]),
                        null,
                        new String[0],
                        new ArrayList<>()
                ), ProxyVersion.BUNGEECORD, Integer.valueOf(values[3]), Integer.valueOf(values[4]), Integer.valueOf(values[5]), new BasicProxyConfig(),
                        ProxyGroupMode.valueOf(values[6]), new WrappedMap());
                this.instance.getCloud().getConfig().createGroup(proxyGroup);
                break;
            case "createwrapper":
                WrapperMeta wrapperMeta = new WrapperMeta(values[0], values[1], values[2]);
                this.instance.getCloud().getConfig().createWrapper(wrapperMeta);
                break;
            case "deleteservergroup":
                if (this.instance.getCloud().getServerGroups().containsKey(values[0])) {
                    this.instance.getCloud().getConfig().deleteGroup(this.instance.getCloud().getServerGroups().get(values[0]));
                    this.instance.getCloud().getServerGroups().remove(values[0]);
                    this.instance.getCloud().getNetworkManager().updateAll();
                    for (MinecraftServer minecraftServer : this.instance.getCloud().getServers(values[0]))
                        minecraftServer.getWrapper().stopServer(minecraftServer);
                } else
                    responseDocument.append("response", "No group found");

                break;
            case "deleteproxygroup":
                if (this.instance.getCloud().getProxyGroups().containsKey(values[0])) {
                    this.instance.getCloud().getConfig().deleteGroup(this.instance.getCloud().getProxyGroups().get(values[0]));
                    this.instance.getCloud().getProxyGroups().remove(values[0]);
                    this.instance.getCloud().getNetworkManager().updateAll();
                    for (ProxyServer minecraftServer : this.instance.getCloud().getProxys(values[0]))
                        minecraftServer.getWrapper().stopProxy(minecraftServer);
                } else
                    responseDocument.append("response", "No group found");
                break;
        }
    }
}

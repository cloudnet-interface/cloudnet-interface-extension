package de.panamo.interfaceextension.messagehandler;


import de.dytanic.cloudnet.lib.player.CloudPlayer;
import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.dytanic.cloudnetcore.database.StatisticManager;
import de.dytanic.cloudnetcore.network.components.MinecraftServer;
import de.dytanic.cloudnetcore.network.components.ProxyServer;
import de.dytanic.cloudnetcore.network.components.Wrapper;
import de.panamo.interfaceextension.InterfaceExtension;
import org.java_websocket.WebSocket;

public class InfoMessageHandler implements MessageHandler {
    private InterfaceExtension instance;

    public InfoMessageHandler(InterfaceExtension instance) {
        this.instance = instance;
    }

    @Override
    public void handle(String message, String[] values, Document responseDocument, WebSocket webSocket, User user) {
        switch (message) {
            case "serverinfos":
                Document response = new Document();
                for (MinecraftServer minecraftServer : this.instance.getCloud().getServers().values())
                    response.append(minecraftServer.getServiceId().getServerId(), minecraftServer.getServerInfo());

                responseDocument.append("response", response);
                break;
            case "proxyinfos":
                Document proxyResponse = new Document();
                for (ProxyServer minecraftServer : this.instance.getCloud().getProxys().values())
                    proxyResponse.append(minecraftServer.getServiceId().getServerId(), minecraftServer.getProxyInfo());

                responseDocument.append("response", proxyResponse);
                break;
            case "userinfo":
                Document userResponse = new Document();
                userResponse.append("name", user.getName());
                userResponse.append("uuid", user.getUniqueId().toString());
                userResponse.append("permissions", user.getPermissions());

                responseDocument.append("response", userResponse);
                break;
            case "onlineplayers":
                Document playerResponse = new Document();
                for (CloudPlayer cloudPlayer : this.instance.getCloud().getNetworkManager().getOnlinePlayers().values())
                    playerResponse.append(cloudPlayer.getUniqueId().toString(), cloudPlayer);

                responseDocument.append("response", playerResponse);
                break;
            case "statistics":
                responseDocument.append("response", StatisticManager.getInstance().getStatistics());
                break;
            case "cloudnetwork":
                responseDocument.append("response", this.instance.getCloud().getNetworkManager().newCloudNetwork());
                break;
            case "memory":
                Document memoryDocument = new Document()
                        .append("maxMemory", this.instance.getCloud().globalMaxMemory())
                        .append("usedMemory", this.instance.getCloud().globalUsedMemory());
                responseDocument.append("response", memoryDocument);
                break;
            case "cpuusage":
                double cpuUsage = 0.0D;
                for (Wrapper wrapper : this.instance.getCloud().getWrappers().values())
                    cpuUsage += wrapper.getCpuUsage();
                cpuUsage /= this.instance.getCloud().getWrappers().size();
                responseDocument.append("response", cpuUsage);
                break;
        }
    }
}

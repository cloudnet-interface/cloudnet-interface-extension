package de.panamo.interfaceextension;


import java.util.UUID;

public class ModuleConfiguration {
    private String tokenSecret = UUID.randomUUID().toString();
    private int port = 1443;


    void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    void setPort(int port) {
        this.port = port;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public int getPort() {
        return port;
    }
}

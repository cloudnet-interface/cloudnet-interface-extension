package de.panamo.interfaceextension.listener;

import de.dytanic.cloudnet.event.IEventListener;
import de.dytanic.cloudnetcore.api.event.network.ChannelInitEvent;
import de.dytanic.cloudnetcore.api.event.server.ProxyAddEvent;
import de.dytanic.cloudnetcore.api.event.server.ProxyRemoveEvent;
import de.dytanic.cloudnetcore.api.event.server.ServerAddEvent;
import de.dytanic.cloudnetcore.api.event.server.ServerRemoveEvent;
import de.dytanic.cloudnetcore.network.components.MinecraftServer;
import de.dytanic.cloudnetcore.network.components.ProxyServer;
import de.panamo.interfaceextension.InterfaceExtension;

public class ServerListener {
    private static InterfaceExtension instance = InterfaceExtension.getInstance();

    public static class ServerAddListener implements IEventListener<ServerAddEvent> {

        @Override
        public void onCall(ServerAddEvent event) {
            instance.getSocketServer().broadcastNotification("Der Server " + event.getMinecraftServer().getName() + " wird gestartet ...");
        }
    }

    public static class ProxyAddListener implements IEventListener<ProxyAddEvent> {

        @Override
        public void onCall(ProxyAddEvent event) {
            instance.getSocketServer().broadcastNotification("Die Proxy " + event.getProxyServer().getName() + " wird gestartet ...");
        }
    }

    public static class ServerRemoveListener implements IEventListener<ServerRemoveEvent> {

        @Override
        public void onCall(ServerRemoveEvent event) {
            String name = event.getMinecraftServer().getName();
            instance.getSocketServer().broadcastNotification("Der Server " + name + " wird gestoppt ...");
            instance.getConsoleSessionHandler().removeLines(name);
        }
    }

    public static class ProxyRemoveListener implements IEventListener<ProxyRemoveEvent> {

        @Override
        public void onCall(ProxyRemoveEvent event) {
            String name = event.getProxyServer().getName();
            instance.getSocketServer().broadcastNotification("Die Proxy " + name + " wird gestoppt ...");
            instance.getConsoleSessionHandler().removeLines(name);
        }
    }

    public static class ChannelInitListener implements IEventListener<ChannelInitEvent> {

        @Override
        public void onCall(ChannelInitEvent event) {
            String name = event.getINetworkComponent().getServerId();
            if(event.getINetworkComponent() instanceof MinecraftServer)
                instance.getSocketServer().broadcastNotification("Der Server " + name + " wurde gestartet");
            else if(event.getINetworkComponent() instanceof ProxyServer)
                instance.getSocketServer().broadcastNotification("Die Proxy " + name + " wurde gestartet");
        }
    }
}

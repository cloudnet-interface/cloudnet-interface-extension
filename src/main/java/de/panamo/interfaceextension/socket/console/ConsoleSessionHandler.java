package de.panamo.interfaceextension.socket.console;

import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.dytanic.cloudnetcore.network.components.MinecraftServer;
import de.dytanic.cloudnetcore.network.components.ProxyServer;
import de.panamo.interfaceextension.InterfaceExtension;
import de.panamo.interfaceextension.messagehandler.MessageHandler;
import org.java_websocket.WebSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConsoleSessionHandler implements MessageHandler {
    private InterfaceExtension instance;
    private Map<WebSocket, SocketConsoleSession> socketConsoleSessions = new HashMap<>();
    private Map<String, List<String>> cachedLogs = new HashMap<>();

    public ConsoleSessionHandler(InterfaceExtension instance) {
        this.instance = instance;
    }

    @Override
    public void handle(String message, String[] values, Document responseDocument, WebSocket webSocket, User user) {
        switch (message) {
            case "serverconsolesession":
                String serverConsoleName = values[0];
                MinecraftServer minecraftServer = this.instance.getCloud().getServer(serverConsoleName);
                if(minecraftServer != null) {
                    if(this.socketConsoleSessions.containsKey(webSocket)) {
                        this.instance.getCloud().getScreenProvider().disableScreen(
                                this.socketConsoleSessions.get(webSocket).getServer());
                        this.socketConsoleSessions.remove(webSocket);
                    } else {
                        minecraftServer.getWrapper().enableScreen(minecraftServer.getServerInfo());
                        this.instance.getCloud().getScreenProvider().setMainServiceId(minecraftServer.getServiceId());

                        SocketConsoleSession socketConsoleSession = new SocketConsoleSession(webSocket, serverConsoleName);
                        this.socketConsoleSessions.put(webSocket, socketConsoleSession);
                        socketConsoleSession.sendExistingLines();
                    }
                } else
                    responseDocument.append("response", "No server found");

                break;
            case "proxyconsolesession":
                String proxyConsoleName = values[0];
                ProxyServer proxyServer = this.instance.getCloud().getProxy(proxyConsoleName);
                if(proxyServer != null) {
                    if(this.socketConsoleSessions.containsKey(webSocket)) {
                        this.instance.getCloud().getScreenProvider().disableScreen(
                                this.socketConsoleSessions.get(webSocket).getServer());
                        this.socketConsoleSessions.remove(webSocket);
                    } else {
                        proxyServer.getWrapper().enableScreen(proxyServer.getProxyInfo());
                        this.instance.getCloud().getScreenProvider().setMainServiceId(proxyServer.getServiceId());

                        SocketConsoleSession socketConsoleSession = new SocketConsoleSession(webSocket, proxyConsoleName);
                        this.socketConsoleSessions.put(webSocket, socketConsoleSession);
                        socketConsoleSession.sendExistingLines();
                    }
                } else
                    responseDocument.append("response", "No proxy found");

                break;
            case "servercommand":
                MinecraftServer commandMinecraftServer = this.instance.getCloud().getServer(values[0]);
                if(commandMinecraftServer != null)
                    commandMinecraftServer.getWrapper().writeServerCommand(values[1], commandMinecraftServer.getServerInfo());
                else
                    responseDocument.append("response", "No server found");
                break;
            case "proxycommand":
                ProxyServer commandProxyServer = this.instance.getCloud().getProxy(values[0]);
                if(commandProxyServer != null)
                    commandProxyServer.getWrapper().writeProxyCommand(values[1], commandProxyServer.getProxyInfo());
                else
                    responseDocument.append("response", "No proxy found");
                break;
        }
    }

    void appendLines(String server, List<String> lines) {
        if(!this.cachedLogs.containsKey(server.toLowerCase()))
            this.cachedLogs.put(server.toLowerCase(), new ArrayList<>());

        this.cachedLogs.get(server.toLowerCase()).addAll(lines);
    }

    List<String> getLines(String server) {
        return this.cachedLogs.get(server.toLowerCase());
    }

    public void removeLines(String server) {
        this.cachedLogs.remove(server.toLowerCase());
    }

    public Map<String, List<String>> getCachedLogs() {
        return cachedLogs;
    }

    public Map<WebSocket, SocketConsoleSession> getSocketConsoleSessions() {
        return socketConsoleSessions;
    }
}

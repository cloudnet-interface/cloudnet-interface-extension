package de.panamo.interfaceextension.socket.console;

import de.dytanic.cloudnet.lib.utility.document.Document;
import de.panamo.interfaceextension.InterfaceExtension;
import org.java_websocket.WebSocket;
import java.util.Arrays;
import java.util.List;

public class SocketConsoleSession {
    private WebSocket webSocket;
    private String server;
    private ConsoleSessionHandler consoleSessionHandler = InterfaceExtension.getInstance().getConsoleSessionHandler();

    SocketConsoleSession(WebSocket webSocket, String server) {
        this.webSocket = webSocket;
        this.server = server;
    }

    void sendExistingLines() {
        List<String> lines = this.consoleSessionHandler.getLines(this.server);
        if(lines != null)
            this.sendLogLines0(lines);
    }

    public void sendLogLines(String... lines) {
        List<String> lineList = Arrays.asList(lines);
        this.consoleSessionHandler.appendLines(this.server, lineList);
        this.sendLogLines0(lineList);
    }

    private void sendLogLines0(List<String> lines) {
        Document document = new Document("message", "consoleLine").append("content", lines);
        this.webSocket.send(document.convertToJsonString());
    }

    public WebSocket getWebSocket() {
        return webSocket;
    }

    public String getServer() {
        return server;
    }
}

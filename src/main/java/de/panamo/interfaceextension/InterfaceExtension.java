package de.panamo.interfaceextension;

import de.dytanic.cloudnetcore.api.CoreModule;
import de.panamo.interfaceextension.listener.ScreenInfoListener;
import de.panamo.interfaceextension.listener.ServerListener;
import de.panamo.interfaceextension.messagehandler.CreateDeleteMessageHandler;
import de.panamo.interfaceextension.messagehandler.InfoMessageHandler;
import de.panamo.interfaceextension.messagehandler.StartStopMessageHandler;
import de.panamo.interfaceextension.socket.InterfaceSocketServer;
import de.panamo.interfaceextension.socket.console.ConsoleSessionHandler;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import java.io.File;
import java.io.IOException;

public class InterfaceExtension extends CoreModule {
    private static InterfaceExtension instance;

    private InterfaceSocketServer socketServer;
    private ConsoleSessionHandler consoleSessionHandler = new ConsoleSessionHandler(this);

    /**
     * Creates and starts the webSocketServer when the module is being enabled
     */

    @Override
    public void onBootstrap() {
        instance = this;

        super.registerListener(new ServerListener.ServerAddListener());
        super.registerListener(new ServerListener.ServerRemoveListener());
        super.registerListener(new ServerListener.ProxyAddListener());
        super.registerListener(new ServerListener.ProxyRemoveListener());
        super.registerListener(new ServerListener.ChannelInitListener());

        super.registerListener(new ScreenInfoListener());

        this.socketServer = new InterfaceSocketServer(this,
                this.loadConfiguration(new File(super.getDataFolder(), "Interface-Extension")));

        this.socketServer.registerHandler(this.consoleSessionHandler);
        this.socketServer.registerHandler(new InfoMessageHandler(this));
        this.socketServer.registerHandler(new StartStopMessageHandler(this));
        this.socketServer.registerHandler(new CreateDeleteMessageHandler(this));

        this.socketServer.start();
    }


    private ModuleConfiguration loadConfiguration(File dataFolder) {
        ModuleConfiguration moduleConfiguration = new ModuleConfiguration();

        File configFile = new File(dataFolder, "config.yml");
        if(!configFile.exists()) {
            try {
                if(!dataFolder.exists())
                    dataFolder.mkdir();

                configFile.createNewFile();
                Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);

                config.set("tokenSecret", moduleConfiguration.getTokenSecret());
                config.set("webSocketServerPort", moduleConfiguration.getPort());

                ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, configFile);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            return moduleConfiguration;
        }

        try {
            Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
            moduleConfiguration.setTokenSecret(config.getString("tokenSecret"));
            moduleConfiguration.setPort(config.getInt("webSocketServerPort"));
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return moduleConfiguration;
    }

    /**
     * Stops the webSocketServer when the module is being disabled
     */

    @Override
    public void onShutdown() {
        try {
            this.socketServer.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ConsoleSessionHandler getConsoleSessionHandler() {
        return consoleSessionHandler;
    }

    public InterfaceSocketServer getSocketServer() {
        return socketServer;
    }

    public static InterfaceExtension getInstance() {
        return instance;
    }
}
